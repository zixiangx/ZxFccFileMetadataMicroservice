﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccFileMetadataMicroservice.Models
{
    public class MetadataModel
    {
        public long Size { get; set; }
        public string SizeFormat { get; set; }
        public string Type { get; set; }
    }
}
