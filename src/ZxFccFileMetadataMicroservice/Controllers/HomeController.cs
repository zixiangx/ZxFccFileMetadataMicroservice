﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ZxFccFileMetadataMicroservice.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ZxFccFileMetadataMicroservice.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Submit(IFormFile fileUpload)
        {
            MetadataModel model = new MetadataModel()
            {
                Size = fileUpload.Length,
                SizeFormat = "bytes",
                Type = fileUpload.Headers.FirstOrDefault(f => f.Key == "Content-Type").Value
            };
            return Content(JsonConvert.SerializeObject(model), "application/json");
        }
    }
}
