This is a .NET Core API microservice that takes a user uploaded file and 
processes it to return meta data of the file to the user in JSON format.

The return data currently includes file size, size unit as well as file type.

The demo can be found at:
http://zxfccfilemetadatamicroservice.azurewebsites.net/